package com.souhaila.flickr.assignement.uinterface;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;

import com.flickr4java.flickr.FlickrException;
import com.souhaila.flickr.assignement.services.FindPicture;
import com.souhaila.flickr.assignement.services.SearchRequest;

import org.eclipse.swt.widgets.FileDialog;

import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;

public class MainWin {

	protected Shell shell;
	private Text text;
	private Text text_1;
	private Text text_2;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {

			MainWin window = new MainWin();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(575, 312);
		shell.setText("Interface Asiignement Part 2 By Souhaila Serbout");

		Label lblNewLabel = new Label(shell, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Ubuntu", 12, SWT.NORMAL));
		lblNewLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblNewLabel.setBounds(35, 29, 98, 29);
		lblNewLabel.setText("Insert a Tag");

		text = new Text(shell, SWT.BORDER);
		text.setBounds(140, 29, 379, 29);

		Button seeButton = new Button(shell, SWT.NONE);
		seeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				DirectoryDialog fd = new DirectoryDialog(shell, SWT.OPEN);
				fd.setText("Import dataset");
				fd.setFilterPath("/");
				String selected = fd.open();
				System.out.println(selected);
				text_2.setText(selected);

			}

		});
		seeButton.setBounds(467, 186, 98, 29);
		seeButton.setText("Save");

		DateTime dateTime = new DateTime(shell, SWT.BORDER);
		dateTime.setBounds(140, 64, 167, 40);

		Label lblNewLabel_1 = new Label(shell, SWT.NONE);
		lblNewLabel_1.setFont(SWTResourceManager.getFont("Ubuntu", 12, SWT.NORMAL));
		lblNewLabel_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblNewLabel_1.setBounds(35, 73, 98, 17);
		lblNewLabel_1.setText("Select Date");

		Button btnCheckButton = new Button(shell, SWT.CHECK);
		btnCheckButton.setBounds(35, 144, 306, 29);
		btnCheckButton.setText("Only phtos with Geolocalization tag");

		Label lblNewLabel_2 = new Label(shell, SWT.NONE);
		lblNewLabel_2.setFont(SWTResourceManager.getFont("Ubuntu", 12, SWT.NORMAL));
		lblNewLabel_2.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblNewLabel_2.setBounds(36, 110, 70, 17);
		lblNewLabel_2.setText("Group ID");

		text_1 = new Text(shell, SWT.BORDER);
		text_1.setBounds(140, 110, 81, 29);

		Label lblNewLabel_3 = new Label(shell, SWT.NONE);
		lblNewLabel_3.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
		lblNewLabel_3.setBounds(10, 195, 124, 29);
		lblNewLabel_3.setText("Destination Folder");

		text_2 = new Text(shell, SWT.BORDER);
		text_2.setBounds(140, 186, 307, 29);

		Button btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.setBounds(426, 232, 117, 29);
		btnNewButton_1.setText("Cancel");
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				shell.close();
			}
		});

		Button btnNewButton = new Button(shell, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				

				if (text.getText().equals("")) {
					
					if (text_2.getText().equals("")) {
					MessageDialog.openError(shell, "Error", "Tag and Destination are obligatory");}
					
					else {
						MessageDialog.openError(shell, "Error", "Tag is obligatory");
					}
				}

				if (text_2.getText().equals("")) {
					if(!text.getText().equals("")) {
					MessageDialog.openError(shell, "Error", "Destination folder is obligatory");}}

				
				if (!text.getText().equals("") ) {
					if (!text_2.getText().equals("")) {
					
				btnNewButton.setEnabled(false);

				SearchRequest searchRequest = new SearchRequest();
				searchRequest.setTags(text.getText());
				searchRequest.setOnlyGeo(btnCheckButton.getSelection());

				searchRequest.setDestinationUri(text_2.getText());
				searchRequest.setGroupID(text_1.getText());
				Date date = new GregorianCalendar(dateTime.getYear(), dateTime.getMonth(), dateTime.getDay()).getTime();
				searchRequest.setDate(date);

				try {

					new FindPicture(searchRequest.getTags(), searchRequest.getDate(), searchRequest.getDestinationUri(),
							searchRequest.isOnlyGeo(), searchRequest.getGroupID());
				} catch (FlickrException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				btnNewButton.setEnabled(true);

				MessageDialog.openInformation(shell, "Info",
						"The pictures are successfully downloaded in the mentioned location");
			}}
			}
		});
		btnNewButton.setBounds(304, 232, 116, 29);
		btnNewButton.setText("Search");

	}
}
