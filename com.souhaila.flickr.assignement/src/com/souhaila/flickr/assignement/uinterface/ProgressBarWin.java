package com.souhaila.flickr.assignement.uinterface;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ProgressBarWin {
	Text text;

	Shell s;

	ProgressBarWin() throws InterruptedException {

		s = new Shell();
		s.setSize(300, 230);

		s.setText("Saving Pictures");
		text = new Text(s, SWT.BORDER);
		text.setText("Saving Photos in the selected directory ..");
		final ProgressBar pb = new ProgressBar(s, SWT.HORIZONTAL);
		pb.setMinimum(0);
		pb.setMaximum(100);
		pb.setSelection(0);

		pb.setBounds(10, 10, 200, 20);

		s.open();

		Thread.sleep(100);
		pb.setSelection(10);
		Thread.sleep(100);
		pb.setSelection(20);
		Thread.sleep(100);
		pb.setSelection(40);
		Thread.sleep(100);
		pb.setSelection(60);
		Thread.sleep(100);
		pb.setSelection(70);
		Thread.sleep(100);
		pb.setSelection(80);
		Thread.sleep(100);
		pb.setSelection(100);

		Thread.sleep(300);
		s.close();
	}

}
