package com.souhaila.flickr.assignement.utilities;


public interface BuddyIconable {
    String getBuddyIconUrl();

    int getIconFarm();

    int getIconServer();

    void setIconFarm(int iconFarm);

    void setIconFarm(String iconFarm);

    void setIconServer(int iconServer);

    void setIconServer(String iconServer);
}
