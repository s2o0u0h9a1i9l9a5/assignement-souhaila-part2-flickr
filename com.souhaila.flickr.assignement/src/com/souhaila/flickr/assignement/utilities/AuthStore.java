/**
 * 
 */
package com.souhaila.flickr.assignement.utilities;

import com.flickr4java.flickr.auth.Auth;

import java.io.IOException;


public interface AuthStore {
    /**
     * Store an Auth.
     * 
     * @param token
     *            Auth object to be stored.
     * @throws IOException
     */
    void store(Auth token) throws IOException;

    /**
     * Retrieve Auth for a given NSID.
     * 
     * @param nsid
     *            NSID
     * @return Auth
     */
    Auth retrieve(String nsid);

    /**
     * Retrieve all Auth objects being stored.
     * 
     * @return Auth objects
     */
    Auth[] retrieveAll();

    /**
     * Clear out the store.
     * 
     */
    void clearAll();

    /**
     * Clear for a given NSID.
     * 
     * @param nsid
     */
    void clear(String nsid);
}
