package com.souhaila.flickr.assignement.services;

import java.util.Date;

/**
 * @author souhaila
 *
 */
public class SearchRequest {

	private String tags;
	private String destinationUri;
	private boolean onlyGeo;
	private String groupID;
	private Date date;

	/**
	 * 
	 */
	public SearchRequest() {
		super();
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @param tags
	 * @param destinationUri
	 * @param onlyGeo
	 */
	public SearchRequest(String tags, String destinationUri, boolean onlyGeo) {
		super();
		this.tags = tags;
		this.destinationUri = destinationUri;
		this.onlyGeo = onlyGeo;
	}

	/**
	 * @return
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	/**
	 * @return
	 */
	public String getDestinationUri() {
		return destinationUri;
	}

	/**
	 * @param destinationUri
	 */
	public void setDestinationUri(String destinationUri) {
		this.destinationUri = destinationUri;
	}

	/**
	 * @return
	 */
	public boolean isOnlyGeo() {
		return onlyGeo;
	}

	/**
	 * @param onlyGeo
	 */
	public void setOnlyGeo(boolean onlyGeo) {
		this.onlyGeo = onlyGeo;
	}

	

}
