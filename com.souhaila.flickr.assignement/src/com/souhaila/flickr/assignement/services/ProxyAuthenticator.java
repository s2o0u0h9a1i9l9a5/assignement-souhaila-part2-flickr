package com.souhaila.flickr.assignement.services;

import java.net.Authenticator;
import java.net.PasswordAuthentication;


public class ProxyAuthenticator extends Authenticator {
    String userName = "";

    String passWord = "";

    public ProxyAuthenticator(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(userName, passWord.toCharArray());
    }
}
