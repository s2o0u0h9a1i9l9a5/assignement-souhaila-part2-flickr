package com.souhaila.flickr.assignement.services;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.lang.System.out;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.imageio.ImageIO;

import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flickr4java.flickr.photos.SearchParameters;
import com.flickr4java.flickr.photos.Size;

public class FindPicture {

	public FindPicture(String tagList, Date date,String uri, boolean geo,String groupID )
			throws FlickrException {
		try {
			String apikey = "0acd664d0d554578c93fd2274fce2a68";
			String secret = "8f8b78ba57efd257";

			Flickr flickr = new Flickr(apikey, secret, new REST());

			SearchParameters searchParameters = new SearchParameters();
			searchParameters.setBBox("-180", "-90", "180", "90");
			searchParameters.setMedia("photos");
			String[] tags = { tagList };
			searchParameters.setTags(tags);
			searchParameters.setGroupId(groupID);
			if (!groupID.equals("")) {
			searchParameters.setHasGeo(geo);}

			Date today = Calendar.getInstance().getTime();

			searchParameters.setInterestingnessDate(date);
			PhotoList<Photo> list = flickr.getPhotosInterface().search(searchParameters, 10, 0);
			String st = "";
			for (String ss : tags) {
				if (st.equals("")) {
					st = st + ss;
				} else {
					st = st + "-" + ss;
				}

				System.out.print(st);
			boolean success = (new File(uri + "/" + st+"("+date.getDay()+"-"+ date.getMonth()+"-"+(date.getYear()+1900)+")")).mkdirs();
			out.println("Image List");
			String information="";
			for (int i = 0; i < list.size(); i++) {
				Photo photo = list.get(i);
				information=information+"Image: " + i + "\n Owner Username: " + photo.getOwner().getUsername() +
						"\n Owner ID: " + photo.getOwner().getId()+
						"\nTitle: " + photo.getTitle() + "\nMedia: " + photo.getOriginalFormat()
				+ "\nPublic: " + photo.isPublicFlag()

				+ "\nUrl: " + photo.getUrl() + "\n";
				
				information=information+"\n";
				out.println("Image: " + i + "\nTitle: " + photo.getTitle() + "\nMedia: " + photo.getOriginalFormat()
						+ "\nPublic: " + photo.isPublicFlag()

						+ "\nUrl: " + photo.getUrl() + "\n");
			}
		
			
			FileWriter writer;
			try {
				writer = new FileWriter(uri + "/" + st +"("+date.getDay()+"-"+ date.getMonth()+"-"+(date.getYear()+1900)+")"+ "/images-info.txt");
		
				writer.write(information);
				writer.close();} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			out.println();

			

			}
			
			for (Photo p : list) {
				int i =0;
				Photo currentPhoto = p;
				PhotosInterface pi = new PhotosInterface(apikey, secret, new REST());

				BufferedImage bufferedImage = pi.getImage(currentPhoto.getUrl());
				// if (bufferedImage!=null) {
				bufferedImage = pi.getImage(currentPhoto, Size.SMALL);

				String title = currentPhoto.getTitle();
				title.replaceAll("/", "-");
				if(title.length()>15) title.substring(0,15);
				
				File outputfile = new File(uri + "/" + st +"("+date.getDay()+"-"+ date.getMonth()+"-"+(date.getYear()+1900)+")"+ "/image-"+p.getId()+".jpg");
				ImageIO.write(bufferedImage, "jpg", outputfile);
			//	ImageIO.write(bufferedImage, "jpg", outputfile);
				i++;
				
				

			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

//	public static void main(String[] args) throws FlickrException {
//		new FindPicture("Plane", null,"/home/souhaila/git/assignement-souhaila-part2-flickr/com.souhaila.flickr.assignement/images", false);
//	}


	
}