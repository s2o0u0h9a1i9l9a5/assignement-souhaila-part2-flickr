package com.souhaila.flickr.assignement.services;


public class FlickrRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1303207981175254196L;

    public FlickrRuntimeException() {
    }

    public FlickrRuntimeException(String message) {
        super(message);
    }

    public FlickrRuntimeException(Throwable rootCause) {
        super(rootCause);
    }

    public FlickrRuntimeException(String message, Throwable rootCause) {
        super(message, rootCause);
    }
}
