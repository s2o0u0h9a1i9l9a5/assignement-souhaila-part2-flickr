package com.souhaila.flickr.assignement.services;

import java.io.File;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Text;

public class SaveThread extends Thread {
	 
    private Display display;
    private ProgressBar progressBar;
    private Button buttonCopy;
    private Button buttonCancel;
    private Label labelInfo;
    private boolean cancel;
	private DateTime dateTime;
	private Text text_1;
	private Text text_2;
	private Button btnCheckButton;
	private Text text;
 
    public SaveThread(Display display, ProgressBar progressBar, //
            Label labelInfo, Button buttonCopy, Button buttonCancel, Text text,Button btnCheckButton, Text text_2,Text text_1,DateTime datetime ) {
        this.display = display;
        this.progressBar = progressBar;
        this.buttonCopy = buttonCopy;
        this.buttonCancel = buttonCancel;
        this.labelInfo = labelInfo;
        this.text=text;
        this.btnCheckButton=btnCheckButton;
        this.text_1=text_1;
        this.text_2=text_2;
        this.dateTime=datetime;
        
    }
 
    @Override
    public void run() {
        if (display.isDisposed()) {
            return;
        }
        this.updateGUIWhenStart();
    	SearchRequest searchRequest= new SearchRequest();
        searchRequest.setTags(text.getText());
		searchRequest.setOnlyGeo(btnCheckButton.getSelection());
		searchRequest.setDestinationUri(text_2.getText());
		searchRequest.setGroupID(text_1.getText());
		Date date=new GregorianCalendar(dateTime.getYear(), dateTime.getMonth(), dateTime.getDay()).getTime();
		searchRequest.setDate(date);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
        this.updateGUIWhenFinish();
    }
 
    private void copy(File file) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
    }
 
    private void updateGUIWhenStart() {
        display.asyncExec(new Runnable() {
 
            @Override
            public void run() {
                buttonCopy.setEnabled(false);
                buttonCancel.setEnabled(true);
            }
        });
    }
 
    private void updateGUIWhenFinish() {
        display.asyncExec(new Runnable() {
 
            @Override
            public void run() {
                buttonCopy.setEnabled(true);
                buttonCancel.setEnabled(false);
                progressBar.setSelection(0);
                progressBar.setMaximum(1);
                if (cancel) {
                    labelInfo.setText("Cancelled!");
                } else {
                    labelInfo.setText("Finished!");
                }
            }
        });
    }
 
    private void updateGUIInProgress( int value, int count) {
        display.asyncExec(new Runnable() {
 
            @Override
            public void run() {
                labelInfo.setText("Saving Pictures ");
                progressBar.setMaximum(count);
                progressBar.setSelection(value);
            }
        });
    }
 
    public void cancel() {
        this.cancel = true;
    }
 
}