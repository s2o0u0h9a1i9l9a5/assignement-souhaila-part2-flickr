

package com.assignement.flick.assignement.photo;

import com.flickr4java.flickr.SearchResultList;


public class PhotoList<E> extends SearchResultList<Photo> {

    // (avoid compiler warning)
    private static final long serialVersionUID = -4735611134085303463L;

}
